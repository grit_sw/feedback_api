from uuid import uuid4

from api import db


class Base(db.Model):
	__abstract__ = True
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
	date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
							  onupdate=db.func.current_timestamp())


class Platform(Base):
	id = db.Column(db.Integer, unique=True,
				   primary_key=True, autoincrement=True)
	name = db.Column(db.String, nullable=False)
	feedbacks = db.relationship('Feedback', backref='platform', lazy='dynamic')

	def __init__(self, name):
		self.name = name.title()

	@staticmethod
	def create(name):
		new_plaform = Platform(name=name)
		db.session.add(new_plaform)
		db.session.commit()
		db.session.refresh(new_plaform)
		return new_plaform

	@staticmethod
	def get_one(name=None, platform_id=None):
		if name:
			name = name.title()
			platform = Platform.query.filter_by(name=name).first()
		elif platform_id:
			platform = Platform.query.filter_by(id=platform_id).first()
		return platform

	@staticmethod
	def get_one_or_create(name):
		name = name.title()
		platform = Platform.query.filter_by(name=name).first()
		if not platform:
			platform = Platform.create(name)
		return platform.to_dict()

	@staticmethod
	def get_all():
		platforms = Platform.query.all()
		return [platform.to_dict() for platform in platforms]

	def to_dict(self):
		payload = {
			'name': self.name,
			'platform_id': self.id,
		}
		return payload


class Feedback(Base):
	id = db.Column(db.Integer, unique=True,
				   primary_key=True, autoincrement=True)
	name = db.Column(db.String, nullable=False)
	email = db.Column(db.String, nullable=False)
	message = db.Column(db.Text, nullable=False)
	feedback_id = db.Column(db.String, nullable=False)
	platform_id = db.Column(db.Integer, db.ForeignKey('platform.id'))

	def __init__(self, name, email, message, platform):
		self.name = name.title()
		self.email = email
		self.message = message
		self.platform = platform
		self.feedback_id = str(uuid4())

	@staticmethod
	def create(name, email, message, platform_id):
		new_feedback = Feedback(
			name=name,
			email=email,
			message=message,
			platform=Platform.get_one(platform_id=platform_id)
		)
		db.session.add(new_feedback)
		db.session.commit()
		db.session.refresh(new_feedback)
		return new_feedback.to_dict()

	@staticmethod
	def by_platform(platform_id, page_number, response_size):
		platform = Platform.query.filter_by(id=platform_id).first()
		feedbacks = platform.feedbacks.paginate(
			page_number, response_size, False)
		all_feedbacks = [feedback.small_dict() for feedback in feedbacks.items]
		return all_feedbacks, feedbacks.has_prev, feedbacks.has_next

	@staticmethod
	def get_all(page_number, response_size):
		feedbacks = Feedback.query.paginate(
			page_number, response_size, False)
		all_feedbacks = [feedback.to_dict() for feedback in feedbacks.items]
		return all_feedbacks, feedbacks.has_prev, feedbacks.has_next

	def small_dict(self):
		payload = {
			'name': self.name,
			'email': self.email,
			'message': self.message,
			'platform_id': self.platform_id,
		}
		return payload

	def to_dict(self):
		payload = {
			'name': self.name,
			'email': self.email,
			'message': self.message,
			'platform': self.platform.to_dict(),
		}
		return payload
