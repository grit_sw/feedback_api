from api.models import Feedback, Platform
from logger import logger
from flask import request


class FeedbackManager(object):
	"""
		Class to manage user creation and retrieval
	"""

	def create(self, payload):
		"""
			Method to perform account legder credit, called after payment
			@args: data payload
			@returns: a status message and status code
		"""
		response = {}
		try:
			role_id = int(request.cookies.get('role_id'))
		except (ValueError, TypeError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised'
			return response, 403

		platform_name = request.user_agent.platform or "Not Provided"
		platform_id = Platform.get_one_or_create(platform_name)['platform_id']
		payload['platform_id'] = platform_id
		try:
			data = Feedback.create(**payload)
			response['success'] = True
			response['data'] = data
			return response, 201

		except Exception as e:
			logger.critical(e)
			raise
			response['success'] = False
			response['message'] = 'Application Error. Staff notified.'
			return response, 400

	def by_platform(self, platform_id, page_number, response_size):
		response = {}
		try:
			role_id = int(request.cookies.get('role_id'))
		except (ValueError, TypeError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised'
			return response, 403
		if not role_id == 5:
			response['success'] = False
			response['message'] = 'Unauthourised'
			return response, 403

		try:
			response_data = Feedback.by_platform(platform_id, page_number, response_size)
			response['success'] = True
			response['data'] = response_data
			return response, 200

		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Application Error. Staff notified.'
			return response, 400

	def get_all(self, page_number, response_size):
		"""
			Method to view all feedbacks.
		"""
		response = {}
		try:
			role_id = int(request.cookies.get('role_id'))
		except (ValueError, TypeError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised'
			return response, 403
		if not role_id == 5:
			response['success'] = False
			response['message'] = 'Unauthourised'
			return response, 403

		try:
			response_data, has_prev, has_next = Feedback.get_all(page_number, response_size)
			response['success'] = True
			response['data'] = response_data
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 200

		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Application Error. Staff notified.'
			return response, 400

	def all_platforms(self):
		"""
			Method to view all platforms
			@args: data payload
			@returns: a response message and status code
		"""
		response = {}
		try:
			role_id = int(request.cookies.get('role_id'))
		except (ValueError, TypeError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised'
			return response, 403
		if not role_id == 5:
			response['success'] = False
			response['message'] = 'Unauthourised'
			return response, 403

		try:
			response_data = Platform.get_all()
			response['success'] = True
			response['data'] = response_data
			return response, 200

		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Application Error. Staff notified.'
			return response, 400

