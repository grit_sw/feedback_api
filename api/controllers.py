from collections import namedtuple

from api import api
from api.manager import FeedbackManager
from flask import request
from flask_restplus import Namespace, Resource, fields
from logger import logger
from marshmallow import Schema, ValidationError, post_load, validates
from marshmallow import fields as ma_fields



Feedback = namedtuple('Feedback', [
	'name',
	'email',
	'message',
])

class FeedbackSchema(Schema):
	name = ma_fields.String(required=True)
	email = ma_fields.String(required=True)
	message = ma_fields.String(required=True)


	@post_load
	def activate(self, data):
		return Feedback(**data)

	@validates('name')
	def validate_name(self, value):
		if len(value) <= 0:
			raise ValidationError('The name of the user who\'s giving the feedback.')

	@validates('email')
	def validate_email(self, value):
		if len(value) <= 0:
			raise ValidationError('The email of the user who\'s giving the feedback.')

	@validates('message')
	def validate_message(self, value):
		if len(value) <= 0:
			raise ValidationError('The feedback message.')


feedback_api = Namespace('Feedback', description='Api for managing application feedbacks')

# For swagger documentation
feedback = feedback_api.model('feedback', {
	'name': fields.String(required=True, description='The name of the user giving the feedback.'),
	'email': fields.String(required=True, description='The email address of the user giving the feedback.'),
	'message': fields.String(required=True, description='The body of the feedback message.'),
})

@feedback_api.route('/new/')
class NewFeedback(Resource):
	@feedback_api.expect(feedback)  # For swagger documentation
	def post(self):
		"""
			HTTP method for creating feedbacks.
		"""
		schema = FeedbackSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data

		response = {}

		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['message'] = e.messages
			return response, 400

		manager = FeedbackManager()
		resp, code = manager.create(new_payload)
		return resp, code


@feedback_api.route('/all/')
class AllFeedbacks(Resource):
	def get(self):
		"""
			get all feedbacks arguments: page_number, response_size
			@param: request payload
			@returns: response and status code
		"""
		manager = FeedbackManager()

		page_number = request.args.get('page_number', 1)
		response_size = request.args.get('response_size', 20)
		resp, code = manager.get_all(page_number, response_size)
		return resp, code


@feedback_api.route('/platforms/all/')
class Platforms(Resource):
	def get(self):
		"""
			get all platforms
			@param: request payload
			@returns: response and status code
		"""
		manager = FeedbackManager()

		resp, code = manager.all_platforms()
		return resp, code


@feedback_api.route('/by-platform/<string:platform_id>/')
class ByPlatform(Resource):
	def get(self, platform_id):
		"""
			feedbacks by platform arguments: page_number, response_size
			@param: request payload
			@returns: response and status code
		"""
		manager = FeedbackManager()

		page_number = request.args.get('page_number', 1)
		response_size = request.args.get('response_size', 20)
		resp, code = manager.by_platform(platform_id, page_number, response_size)
		return resp, code
