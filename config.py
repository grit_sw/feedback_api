import os


class Config(object):
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.getenv('SECRET_KEY')

    @staticmethod
    def init_app(app):
        pass

class Development(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    # SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost/gtg_accounts'

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)

    @staticmethod
    def init_app(app, *args):
        pass


class Testing(Config):
    """TODO"""
    pass

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)


class Staging(Config):
    """TODO"""
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)


class Production(Config):
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    RESTPLUS_MASK_SWAGGER = True

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)


config = {
    'Development': Development,
    'Testing': Testing,
    'Production': Production,
    'Staging': Staging,
    'default': Development,
}
