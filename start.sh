#!/bin/sh

source venv/bin/activate

sleep 10

export FLASK_APP=run_api.py

echo 'Running upgrade'
# flask db upgrade

# exec flask run -p 5000
# exec gunicorn -w 4 -b :5000 --access-logfile - --error-logfile - run_api:app --timeout $TIMEOUT
exec gunicorn -b :5000 -k eventlet --access-logfile - --error-logfile - run_api:app --workers $NUM_WORKERS --timeout $TIMEOUT
